# PokeGlow Data

The application's main entity is the "glober". Each glober will have a few attributes and an array of "questions" that will be browsed by the user using the app's controls.

## Model

### NPC

```json
{
  "name": "Mariana de Fátima",
  "position": "Champion",
  "site": "MX",
  "picture": "foo/bar.png",
  "questions":
  [
    {
    "1":
      {
        "call_to_action": "Why am I here?",
        "type": "video",
        "content": "youtube",
      }
    },
    {
    "5":
      {
        "call_to_action": "What's love got to do with it?",
        "type": "text",
        "content": "contenido",
      }
    }
  ]
}
```
